'use strict'
const imgs = document.querySelectorAll('.image-to-show');

let img = 0;

function slider() {
    if (img === imgs.length - 1) {
        imgs[img].style.display = 'none';
        img = 0;
        imgs[0].style.display = 'block';
    } else {
        imgs[img].style.display = 'none';
        imgs[img + 1].style.display = 'block';
        img++;
    }
}
let moveSlider = setInterval(slider, 3000);

const btns = document.querySelector('.wrapper-btn');
setTimeout(() => {btns.style.visibility = 'visible';}, 3000);

const stopBtn = document.querySelector('.btn-stop');
stopBtn.addEventListener('click', function () {
    clearInterval(moveSlider);
    stopBtn.setAttribute('disabled', '');
    continueBtn.removeAttribute('disabled');
})

const continueBtn = document.querySelector('.btn-continue');
continueBtn.setAttribute('disabled', '');

continueBtn.addEventListener('click', function () {
    moveSlider = setInterval(slider, 3000);
    continueBtn.setAttribute('disabled', '');
    stopBtn.removeAttribute('disabled');
})